# elFinder builds

This is a built regularly from the 2.1 branch from elFinder: https://github.com/Studio-42/elFinder/tree/2.1

This build had a customization, so that the thumbs generated keep the original file name, instead of encrypted.


# Add this repo to root composer

```
"repositories": [
        {
            "type": "package",
            "package": {
                "name": "barryvdh/elfinder-builds",
                "version": "2.1.9",
                "source": {
                    "url": "https://iboinas@bitbucket.org/itperformance/elfinder-builds-custom.git",
                    "type": "git",
                    "reference": "v2.1.9"
                },
                "autoload": {
                    "classmap": ["php"]
                }
            }
        },
```